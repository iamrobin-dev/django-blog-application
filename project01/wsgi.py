"""
WSGI config for project01 project.

It exposes the WSGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/2.2/howto/deployment/wsgi/
"""

import os

from django.core.wsgi import get_wsgi_application
# project01.settings.pro for running gunicon on pro mode and local for local mode
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'project01.settings.local')

application = get_wsgi_application()
