from django.views.generic import TemplateView, FormView, ListView
from django.shortcuts import render
from .forms import ContactForm
from .models import About


class IndexView(TemplateView):
    template_name = "pages/index.html"


class AboutView(ListView):
    model = About
    template_name = 'pages/about.html'
    context_object_name = 'about'


class ContactView(FormView):
    template_name = 'pages/contact.html'
    form_class = ContactForm
    # fields = ['name', 'email', 'subject', 'message']
