from django.shortcuts import render
from django.views.generic import ListView, DetailView, CreateView, UpdateView, DeleteView
from django.utils import timezone
from .models import Post
from django.utils.text import slugify
from django.contrib.auth.mixins import LoginRequiredMixin, UserPassesTestMixin


class PostListView(ListView):
    queryset = Post.objects.all().order_by('-created_on').filter(status='published')
    model = Post
    template_name = 'blogs/post_list.html'
    # ordering = ['-created_on']
    context_object_name = 'posts'
    # paginate_by = 5

    # def get_queryset(self):
    #     return Post.objects.all().order_by('-created_on').filter(status='published')


class PostDetailView(DetailView):
    model = Post


class PostCreateView(LoginRequiredMixin, CreateView):
    model = Post
    fields = ['title', 'status', 'content']

    # def get_object(self):
    #     d = self.request.status
    #     print(d)

    def form_valid(self, form):
        form.instance.author = self.request.user

        # need to slugify title in views
        # slug = slugify(form.instance.title)
        # form.save()
        return super().form_valid(form)


class PostUpdateView(LoginRequiredMixin, UserPassesTestMixin, UpdateView):
    model = Post
    fields = ['title', 'content']

    def form_valid(self, form):
        form.instance.author = self.request.user
        return super().form_valid(form)

    def test_func(self):
        post = self.get_object()
        if self.request.user == post.author:
            return True
        return False


class PostDeleteView(LoginRequiredMixin, UserPassesTestMixin, DeleteView):
    model = Post
    success_url = '/'

    def test_func(self):
        post = self.get_object()
        if self.request.user == post.author:
            return True
        return False
