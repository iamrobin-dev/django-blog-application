from django.contrib import admin
from .models import Post
# Register your models here.


class PostModel(admin.ModelAdmin):
    list_display = ('title', 'author', 'status', 'created_on',
                    'updated_on', 'content_description')
    date_hierarchy = 'created_on'
    # prepopulated_fields = {'slug': ('title',)}
    ordering = ('-updated_on',)


admin.site.register(Post, PostModel)
