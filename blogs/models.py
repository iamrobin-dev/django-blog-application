from django.db import models
from django.contrib.auth.admin import User
from django.template.defaultfilters import truncatechars
from django.urls import reverse
from ckeditor.fields import RichTextField


class Post(models.Model):
    STATUS_CHOICES = (('draft', 'Draft'), ('published', 'Published'))
    author = models.ForeignKey(
        User, on_delete=models.CASCADE, related_name='blog_posts')
    title = models.CharField(max_length=100)
    # slug = models.SlugField(max_length=200, unique=True,
    #                         null=True)

    content = RichTextField(null=True, blank=True)
    status = models.CharField(
        max_length=30, choices=STATUS_CHOICES, default='published')
    created_on = models.DateTimeField(auto_now_add=True)
    updated_on = models.DateTimeField(auto_now=True)

    def content_description(self):
        return truncatechars(self.content, 50)

    def __str__(self):
        return self.title

    def get_absolute_url(self):
        return reverse('post_detail', kwargs={'pk': self.pk})
